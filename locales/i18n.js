import i18n from 'i18next';
import { reactI18nextModule } from 'react-i18next';
import Expo from 'expo';
import enJson from './en.json';
import zhJson from './zh.json';
import msJson from './ms.json';
// creating a language detection plugin using expo
// http://i18next.com/docs/ownplugin/#languagedetector

const languageDetector = {
    type: 'languageDetector',
    async: true, // flags below detection to be async
    detect: (callback) => {
        return /*'en'; */ Expo.Util.getCurrentLocaleAsync().then(lng => {
            const lang = lng.replace('_', '-');
            console.log("selected locale: " + lang);
            callback(lang);
        })
    },
    init: () => { },
    cacheUserLanguage: () => { }
}

i18n
    .use(languageDetector)
    .use(reactI18nextModule)
    .init({
        fallbackLng: 'en',

        resources: {
            en: enJson,
            zh: zhJson,
            ms: msJson
        },

        // have a common namespace used around the full app
        ns: ['common'],
        defaultNS: 'common',

        debug: true,

        // cache: {
        //   enabled: true
        // },

        interpolation: {
            escapeValue: false, // not needed for react as it does escape per default to prevent xss!
        }
    });


export default i18n;