import React, { Component } from 'react';
import { Platform, View, Text, FlatList } from 'react-native';

import styles from '../screens/styles/styles';
import SingleLineItem from './components/SingleLineItem';

class HomeScreen extends Component {
    static navigationOptions = () => {
        return {
            header: null
        };
    };

    onItemPressed(item) {
        // get first char at lower case and following string
        const key = item.key.charAt(0).toLowerCase() + item.key.slice(1);
        this.props.navigation.navigate(key + "Screen");
    }

    render() {
        items = [
            { title: 'Single Line Item', key: 'SingleLineItem' },
            { title: 'Single Line Icon Item', key: 'SingleLineIconItem' },
            { title: 'Single Line Icon Checkbox Item', key: 'SingleLineIconCheckboxItem' },
            { title: 'Two Line Item', key: 'TwoLineItem' },
            { title: 'Two Line Single Selection Item', key: 'TwoLineSingleSelectionItem' },
            { title: 'Threshold', key: 'Threshold' },
            { title: 'Option Button', key: 'OptionButton' },
            { title: 'Option Switch', key: 'OptionSwitch' },
        ];

        return (
            <View style={styles.container}>
                <FlatList
                    data={items}
                    renderItem={
                        ({ item }) =>
                            <SingleLineItem
                                item={item}
                                readOnly={false}
                                onPress={item => this.onItemPressed(item)}
                            />
                    }
                />
            </View>
        );
    }
}

export default HomeScreen;
