import { Platform, StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import COLORS from './colors';

const window = Dimensions.get('window');

export const SCREEN_WIDTH = window.width;
export const IMAGE_HEIGHT = window.width / 3;

export default StyleSheet.create({
    container: {
        flex: 1,
        ...Platform.select({
            ios: {
                paddingTop: scale(24)
            },
            android: {
                paddingTop: 0
            }
        }),
    },
    centerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoText: {
        fontSize: moderateScale(36),
        margin: scale(8),
        color: 'white',
        textAlign: 'center'
    },
    headerText: {
        fontSize: moderateScale(36),
        color: COLORS.TEXT_HEADER,
        margin: scale(8)
    },
    titleText: {
        fontSize: moderateScale(30),
        fontWeight: 'bold',
        color: COLORS.TEXT_SECONDARY,
        margin: scale(8)
    },
    logo: {
        height: IMAGE_HEIGHT,
        resizeMode: 'contain',
        marginBottom: verticalScale(20),
        padding: scale(10),
        marginTop: verticalScale(20)
    },
    roundedButtonStyle: {
        backgroundColor: COLORS.ACCENT_COLOR,
        borderRadius: scale(20)
    },
    roundedTextInput: {
        height: verticalScale(50),
        backgroundColor: '#fff',
        marginHorizontal: scale(10),
        marginVertical: verticalScale(5),
        borderColor: COLORS.TEXT_HEADER,
        borderWidth: scale(1),
        borderRadius: scale(20),
        paddingVertical: verticalScale(5),
        paddingHorizontal: scale(15),
        width: window.width - scale(70)
    },
    textInput: {
        height: verticalScale(40),
        backgroundColor: '#fff',
        marginHorizontal: scale(10),
        marginVertical: verticalScale(5),
        borderColor: COLORS.TEXT_SECONDARY,
        borderWidth: scale(0.5),
        borderRadius: scale(8),
        paddingVertical: verticalScale(5),
        paddingHorizontal: scale(15),
        width: window.width - scale(30)
    },
    listPrimaryTextStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY,
        margin: scale(4)
    },
    listSecondaryTextStyle: {
        fontSize: moderateScale(14),
        color: COLORS.TEXT_SECONDARY,
        margin: scale(4)
    }
});