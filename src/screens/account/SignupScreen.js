import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { View, Text, Platform, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import styles, { SCREEN_WIDTH } from '../styles/styles';
import COLORS from '../styles/colors';

import AppIcon from '../../../assets/icon.png';
import appInfo from '../../../app.json';

// using the translation hoc to provie t function in props
// https://react.i18next.com/components/translate-hoc.html
@translate(['common', 'signup'], { wait: true })
class SignupScreen extends Component {
    render() {
        const { t, i18n, navigation } = this.props;

        return (
            <View style={[
                styles.centerContainer,
                {
                    backgroundColor: COLORS.THEME_COLOR
                }
            ]}>
                <Image source={AppIcon} style={styles.logo} />
                <Text
                    style={styles.logoText}
                >
                    {t('common:app_name')}
                </Text>

                <Button
                    textStyle={{ color: COLORS.ACCENT_COLOR }}
                    buttonStyle={[
                        customStyles.wideButtonStyle,
                        { backgroundColor: 'white' }
                    ]}
                    title={t('signup:create_account')}
                    onPress={() => { this.props.navigation.navigate('register'); }}
                />

                <Button
                    small
                    style={{ marginTop: verticalScale(100) }}
                    backgroundColor='transparent'
                    underlayColor='transparent'
                    title={t('signup:existing_user')}
                    onPress={() => { this.props.navigation.navigate('login'); }}
                />

                <Button
                    small
                    backgroundColor='transparent'
                    underlayColor='transparent'
                    title={t('signup:skip_login')}
                    onPress={() => { this.props.navigation.navigate('main'); }}
                />

            </View>
        );
    }
}

const customStyles = {
    wideButtonStyle: {
        width: SCREEN_WIDTH - scale(60),
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export default SignupScreen;
