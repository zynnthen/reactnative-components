import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';

import TwoLineItem from '../components/TwoLineItem';

class TwoLineItemScreen extends Component {
    static navigationOptions = () => {
        return {
            title: 'Two Line Item'
        };
    };

    render() {
        items = [
            { primaryText: 'Test 1', secondaryText: 'Aloha', key: 'item1' },
            { primaryText: 'Test 2', secondaryText: 'Hello', key: 'item2' },
            { primaryText: 'Test 3', secondaryText: 'Konichiwa', key: 'item3' },
            { primaryText: 'Test 4', secondaryText: 'Ni hao', key: 'item4' },
            { primaryText: 'Test 5', secondaryText: 'Bonjour', key: 'item5' },
        ];

        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={items}
                    renderItem={
                        ({ item }) =>
                            <TwoLineItem
                                item={item}
                                readOnly={true}
                            />
                    }
                />
            </View>
        );
    }
}

export default TwoLineItemScreen;