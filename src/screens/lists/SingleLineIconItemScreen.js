import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';

class SingleLineIconItemScreen extends Component {
    render() {
        return (
            <View style={{
                flex: 1
            }}>
                <Text>Single Line Icon Item Screen</Text>
            </View>
        );
    }
}

export default SingleLineIconItemScreen;