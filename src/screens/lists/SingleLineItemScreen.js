import React, { Component } from 'react';
import { View, FlatList } from 'react-native';

import SingleLineItem from '../components/SingleLineItem';

class SingleLineItemScreen extends Component {
    static navigationOptions = () => {
        return {
            title: 'Single Line Item'
        };
    };

    render() {
        items = [
            { title: 'Test 1', key: 'item1' },
            { title: 'Test 2', key: 'item2' },
            { title: 'Test 3', key: 'item3' },
            { title: 'Test 4', key: 'item4' },
            { title: 'Test 5', key: 'item5' }
        ];

        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    data={items}
                    renderItem={
                        ({ item }) =>
                            <SingleLineItem
                                item={item}
                                readOnly={true}
                            />
                    }
                />
            </View>
        );
    }
}

export default SingleLineItemScreen;