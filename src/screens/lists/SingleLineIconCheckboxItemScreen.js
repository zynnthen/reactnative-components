import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';

class SingleLineIconCheckboxItemScreen extends Component {
    render() {
        return (
            <View style={{
                flex: 1
            }}>
                <Text>Single Line Icon Checkbox Item Screen</Text>
            </View>
        );
    }
}

export default SingleLineIconCheckboxItemScreen;