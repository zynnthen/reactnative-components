import React, { Component } from 'react';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from '../styles/styles';

class SingleLineItem extends Component {
    static defaultProps = {
        onPress: () => { }
    }

    lineItem() {
        return (
            <View style={{ padding: scale(8) }}>
                <Text style={[styles.listPrimaryTextStyle]}>
                    {this.props.item.title}
                </Text>
            </View>
        );
    }

    onRowPress() {
        this.props.onPress(this.props.item);
    }

    render() {
        if (this.props.readOnly) {
            return (
                this.lineItem()
            );
        } else {
            return (
                <TouchableOpacity onPress={this.onRowPress.bind(this)}>
                    {this.lineItem()}
                </TouchableOpacity>
            );
        }
    }
}

export default SingleLineItem;