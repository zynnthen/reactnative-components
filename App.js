import React from 'react';
import { I18nextProvider, translate } from 'react-i18next';
import { StyleSheet, View } from 'react-native';
import { StackNavigator } from 'react-navigation';

// the locale file
import i18n from './locales/i18n';

import COLORS from './src/screens/styles/colors';

import LoginScreen from './src/screens/account/LoginScreen';
import RegisterScreen from './src/screens/account/RegisterScreen';
import SignupScreen from './src/screens/account/SignupScreen';

import HomeScreen from './src/screens/HomeScreen';
import SingleLineItemScreen from './src/screens/lists/SingleLineItemScreen';
import SinglLineIconItemScreen from './src/screens/lists/SingleLineIconItemScreen';
import SingleLineIconCheckboxItemScreen from './src/screens/lists/SingleLineIconCheckboxItemScreen';
import TwoLineItemScreen from './src/screens/lists/TwoLineItemScreen';

export default class App extends React.Component {
  render() {
    const AccountNavigator = StackNavigator({
      signup: { screen: SignupScreen },
      register: { screen: RegisterScreen },
      login: { screen: LoginScreen }
    }, {
        navigationOptions: ({ navigation, screenProps }) => ({
          headerStyle: { backgroundColor: COLORS.THEME_COLOR, borderWidth: 0, borderBottomColor: COLORS.THEME_COLOR },
          headerTitleStyle: { color: 'white' },
          headerBackTitleStyle: 'white',
          headerTintColor: 'white'
        })
      });

    const HomeNavigator = StackNavigator({
      home: { screen: HomeScreen },
      singleLineItemScreen: { screen: SingleLineItemScreen },
      singleLineIconItemScreen: { screen: SinglLineIconItemScreen },
      singleLineIconCheckboxItemScreen: { screen: SingleLineIconCheckboxItemScreen },
      twoLineItemScreen: { screen: TwoLineItemScreen }
    });

    const MainNavigator = StackNavigator({
      account: { screen: AccountNavigator },
      main: { screen: HomeNavigator }
    }, {
        headerMode: 'none',
        // mode: 'modal',
        // initialRouteName: signedIn ? "SignedIn" : "SignedOut"
      });

    // Wrapping a stack with translation hoc asserts we trigger new render on language change
    // the hoc is set to only trigger rerender on languageChanged
    const WrappedStack = () => {
      return <Stack screenProps={{ t: i18n.getFixedT() }} />;
    }
    const ReloadAppOnLanguageChange = translate('translation', {
      bindI18n: 'languageChanged',
      bindStore: false
    })(WrappedStack);

    return (
      <I18nextProvider i18n={i18n}>
        <View style={{ flex: 1 }}>
          <MainNavigator />
        </View>
      </I18nextProvider>
    );
  }
}